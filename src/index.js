import React from "react";
import ReactDOM from "react-dom/client";

// react dasar
// import App from './App';
// import Variable from './Variable';
// import StateProps from './StateProps';
// import Map from './Map';
// import LifeCycle from './LifeCycle';
import "bootstrap/dist/css/bootstrap.min.css";
import CRUD from "./CRUD";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <CRUD />
  </React.StrictMode>
);
