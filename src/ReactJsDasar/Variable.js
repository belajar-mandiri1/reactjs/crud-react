import React from 'react'

// //const Block Scope
const harga = 3000;

// //Var
// var harga = 20000;
// if(true){
//     var harga = 10000;
// }

// //Let Block Scope
// let harga = 20000;
// if(true){
//     let harga = 10000;
// }

const Variable = () => {
  return (
    <div>
        <h2>Harga : {harga}</h2>  
    </div>
  )
}

export default Variable
