// import React, { Component } from 'react'
//Class Component untuk proyek besar
// export default class Navbar extends Component {
//   render() {
//     return (
//       <div>
//         <h2>Navbar</h2>
//       </div>
//     )
//   }
// }


//function component
// import React from 'react'

// export default function Navbar() {
//   return (
//     <div>
//         <h2>Halo</h2>
//     </div>
//   )
// }


//arrow function untuk proyek kecil
import React from 'react'

const Navbar = () => {
  return (
    <div>
        <h2>Navbar</h2>
    </div>
  )
}

export default Navbar;


