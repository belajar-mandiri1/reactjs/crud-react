import React from "react";

const makanan = [
  {
    nama: "Soto",
    harga: 12000,
  },
  {
    nama: "Bakso",
    harga: 10000,
  },
  {
    nama: "Mie Ayam",
    harga: 8000,
  },
  {
    nama: "Nasgor",
    harga: 15000,
  },
];

//Reduce
const total_bayar = makanan.reduce((total_harga, makanan) => {
  return total_harga + makanan.harga;
}, 0);

const Map = () => {
  return (
    <div>
      <h2>Map Sederhana</h2>
      <ul>
        {makanan.map((makanan, index) => (
          <li>
            {index + 1}. {makanan.nama} - {makanan.harga}
          </li>
        ))}
      </ul>

      <h2>Map Filter Harga Yang lebih dari 11.000</h2>
      <ul>
        {makanan
          .filter((makanan) => makanan.harga > 10000)
          .map((makanan, index) => (
            <li>
              {index + 1}. {makanan.nama} - {makanan.harga}
            </li>
          ))}
      </ul>

      <h3>Total Harga : {makanan.harga} - {total_bayar}</h3>
    </div>
  );
};

export default Map;
