import React, { Component } from 'react'

export default class Sublifecycle extends Component {

    componentWillUnmount() {
        this.props.ubahMakanan("Babi")
    }

  render() {
    return (
      <div>
        <h2>Component sublifecycle</h2>
      </div>
    )
  }
}
